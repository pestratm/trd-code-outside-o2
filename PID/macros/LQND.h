#ifndef LXD_H_
#define LXD_H_

#include "TCanvas.h"
#include "TF1.h"
#include "TF1Convolution.h"
#include "TGraph.h"
#include "TH1D.h"
#include "TH1I.h"
#include "TH2D.h"
#include "THStack.h"
#include "TLegend.h"
#include "TMultiGraph.h"
#include "TSpline.h"
#include "TString.h"
#include "TRandom.h"
#include "TStyle.h"
#include "TLine.h"

#include <cstdint>
#include <memory>
#include <numeric>
#include <variant>
#include <vector>
#include <random>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>

namespace o2::trd::pid
{
std::string const default_chars =
  "abcdefghijklmnaoqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

std::string random_string(size_t len = 4, std::string const& allowed_chars = default_chars)
{
  std::mt19937_64 gen{std::random_device()()};

  std::uniform_int_distribution<size_t> dist{0, allowed_chars.length() - 1};

  std::string ret;

  std::generate_n(std::back_inserter(ret), len, [&] { return allowed_chars[dist(gen)]; });
  return ret;
}

// using declarations
// using Charges = std::array<unsigned char, 18>;
using Charges = std::array<float, 18>;

static constexpr int MAX_CHARGE = 317;
static constexpr int NUM_CHARGES = 3;
static constexpr int NLAYERS = 6;
static constexpr int NREBIN = 1;
static constexpr int NBINS = MAX_CHARGE / NREBIN + 1;
static constexpr int NLLBINS = 400;
static constexpr double SPLINE_RES = 0.02;
static constexpr double SPLINE_POINTS = MAX_CHARGE / SPLINE_RES;

enum kType {
  Electron,
  Positron,
  PionNegative,
  PionPositive,
};

// Base class for ND-LikeLihood methods (N=dimension).
// To recap, what should be the input.
// q0/q1 are 7 bits e.g 0..127, describing the peak and the TR tail.
// q2 is 6 bits e.g 0..63, describing the plateau.
// A LUT is calculated for every layer (0..5), then averaged
// to form on combined LUT per ND.
// Meaning:
//   1D: one LUT
//   2D: two LUTs
//   3D: three LUTs
//
// The LUT are calculated by first applying a spline/fit to the respective
// histograms then evaluating the splines at different points on the x axis
// (a resolution is provided with SPLINE_RES).
// The averaging of the LUTs from different layers is simply.
// Two cut offs should be provided.
// One for smaller values since the electron charge distribution is expected two
// be higher than the pion charge distribution. This makes calculating the LUT
// particulary akward small values. The LUT can be set to zero here.
// The other cut-off must be provided for higher values since the LUTs in
// different Layers flucuate here strongly. Taking the last LUT value before the
// cut-off does the the trick (approximating the average).
template <uint8_t nDim>
class LQND
{

  // Check dimension.
  static_assert(nDim == 1 || nDim == 3,
                "Only 1-3D Likelihood methods are implemented!");

 public:
  // Configure Binning of histograms.
  void makeHistograms()
  {
    printf("Creating Histograms for %uD\n", nDim);
    mLLNegativeStack = new THStack();
    mLLNegativeStack->SetName(Form("electron_ll_stack_%s", mMom));
    mLLNegativeLegend = new TLegend();
    mLLNegativeDistributions[0] =
      new TH1D(Form("electron_ll_distribution_%u_%s", nDim, mMom),
               "", NLLBINS, 0.0, 1.0);
    mLLNegativeDistributions[0]->SetFillStyle(3353);
    mLLNegativeDistributions[0]->SetFillColor(kBlue);
    mLLNegativeStack->Add(mLLNegativeDistributions[0]);
    mLLNegativeLegend->AddEntry(mLLNegativeDistributions[0], "Electron",
                                "l");
    mLLNegativeDistributions[1] = new TH1D(
      Form("pion_negative_ll_distribution_%u_%s", nDim, mMom),
      "PionNegative LL Distribution", NLLBINS, 0.0, 1.0);
    mLLNegativeStack->Add(mLLNegativeDistributions[1]);
    mLLNegativeLegend->AddEntry(mLLNegativeDistributions[1],
                                "PionNegative", "l");

    mLLPositiveStack = new THStack();
    mLLPositiveStack->SetName(Form("positron_ll_stack_%s", mMom));
    mLLPositiveLegend = new TLegend();
    mLLPositiveDistributions[0] =
      new TH1D(Form("positron_ll_distribution_%u_%s", nDim, mMom),
               "", NLLBINS, 0.0, 1.0);
    mLLPositiveDistributions[0]->SetFillStyle(3353);
    mLLPositiveDistributions[0]->SetFillColor(kBlue);
    mLLPositiveStack->Add(mLLPositiveDistributions[0]);
    mLLPositiveLegend->AddEntry(mLLPositiveDistributions[0], "Positron",
                                "l");
    mLLPositiveDistributions[1] = new TH1D(
      Form("pion_positive_ll_distribution_%u_%s", nDim, mMom),
      "PionPositive LL Distribution", NLLBINS, 0.0, 1.0);
    mLLPositiveStack->Add(mLLPositiveDistributions[1]);
    mLLPositiveLegend->AddEntry(mLLPositiveDistributions[1],
                                "PionPositive", "l");

    mEfficiency = new TMultiGraph();
    mEffLegend = new TLegend();
    mEfficiency->SetName(Form("pion_efficiency_%u_%s", nDim, mMom));
    mPionEfficiency[0] = new TGraph();
    mPionEfficiency[0]->SetNameTitle(Form("pion_efficiency_electron_%u_%s", nDim, mMom),
                                     "Electron Eff. vs. Pion Eff.");
    mPionEfficiency[0]->GetXaxis()->SetTitle("electron efficiency");
    mPionEfficiency[0]->GetYaxis()->SetTitle("pion efficiency");
    mPionEfficiency[0]->SetMarkerColor(kBlue);
    mPionEfficiency[0]->SetMarkerStyle(21);
    mEfficiency->Add(mPionEfficiency[0]);
    mEffLegend->AddEntry(mPionEfficiency[0], "negative charge", "p");
    mPionEfficiency[1] = new TGraph();
    mPionEfficiency[1]->SetNameTitle(Form("pion_efficiency_positron_%u_%s", nDim, mMom),
                                     "Positron Eff. vs. Pion Eff.");
    mPionEfficiency[1]->GetXaxis()->SetTitle("positron efficiency");
    mPionEfficiency[1]->GetYaxis()->SetTitle("pion efficiency");
    mPionEfficiency[1]->SetMarkerColor(kRed);
    mPionEfficiency[1]->SetMarkerStyle(21);
    mEfficiency->Add(mPionEfficiency[1]);
    mEffLegend->AddEntry(mPionEfficiency[1], "positive charge", "p");

    m90EffLineNeg = new TLine(0.0, 0.0, 0.0, 0.01);
    m90EffLinePos = new TLine(0.0, 0.0, 0.0, 0.01);
    for (int iDim = 0; iDim < nDim; ++iDim) {
      for (int iLayer = 0; iLayer < NLAYERS; ++iLayer) {
        mElectronDistributions[iDim * NLAYERS + iLayer] =
          new TH1D(
            TString::Format("electron_distribution_%u_%u_layer_%u_%s",
                            iDim + 1, nDim, iLayer, mMom),
            TString::Format(
              "Electron Charge Distribution (Dim%u/%u)- Layer %u",
              iDim + 1, nDim, iLayer),
            NBINS, 0, MAX_CHARGE);
        mPionNegativeDistributions[iDim * NLAYERS + iLayer] =
          new TH1D(
            TString::Format("pion_negative_distribution_%u_%u_layer_%u_%s",
                            iDim + 1, nDim, iLayer, mMom),
            TString::Format(
              "PionNegative Charge Distribution (Dim%u/%u)- Layer %u",
              iDim + 1, nDim, iLayer),
            NBINS, 0, MAX_CHARGE);
        mPositronDistributions[iDim * NLAYERS + iLayer] =
          new TH1D(
            TString::Format("positron_distribution_%u_%u_layer_%u_%s",
                            iDim + 1, nDim, iLayer, mMom),
            TString::Format(
              "Positron Charge Distribution (Dim%u/%u)- Layer %u",
              iDim + 1, nDim, iLayer),
            NBINS, 0, MAX_CHARGE);
        mPionPositiveDistributions[iDim * NLAYERS + iLayer] =
          new TH1D(
            TString::Format("pion_positive_distribution_%u_%u_layer_%u_%s",
                            iDim + 1, nDim, iLayer, mMom),
            TString::Format(
              "PionPositive Charge Distribution (Dim%u/%u)- Layer %u",
              iDim + 1, nDim, iLayer),
            NBINS, 0, MAX_CHARGE);
      }
    }
  }

  // Depending on nDim, we take different steps.
  virtual void addEntry(const enum kType type,
                        const Charges& charges, const uint8_t good) noexcept = 0;

  // Calculate the likelihood and add it to the distribution.
  // This clears the likelihood distribution buffer.
  virtual void addLL(const enum kType type,
                     const Charges& charges, const uint8_t good) noexcept = 0;

  // Finalize all calculations.
  // Meaning:
  //  1. calculate likelihood(s) on all layers
  //  2. apply if needed cut offs
  //  3. make average LUT(s)
  void finalizeEntries()
  {
    printf("Finalizing calculations for %uD\n", nDim);
    // Iterate through each dimension
    for (int iDim = 0; iDim < nDim; ++iDim) {
      // Iterate through each layer
      for (int iLayer = 0; iLayer < NLAYERS; ++iLayer) {
        // Calculate the fit for each layer
        mElectronFits[iDim * NLAYERS + iLayer] =
          makeFit(mElectronDistributions[iDim * NLAYERS + iLayer], iDim);
        mPionNegativeFits[iDim * NLAYERS + iLayer] = makeFit(
          mPionNegativeDistributions[iDim * NLAYERS + iLayer], iDim);
        mPositronFits[iDim * NLAYERS + iLayer] =
          makeFit(mPositronDistributions[iDim * NLAYERS + iLayer], iDim);
        mPionPositiveFits[iDim * NLAYERS + iLayer] = makeFit(
          mPionPositiveDistributions[iDim * NLAYERS + iLayer], iDim);

        // Calculate the LUTs for each layer
        mElectronLUTs[iDim * NLAYERS + iLayer] =
          makeLUT(mElectronFits[iDim * NLAYERS + iLayer],
                  mPionNegativeFits[iDim * NLAYERS + iLayer]);
        mPositronLUTs[iDim * NLAYERS + iLayer] =
          makeLUT(mPositronFits[iDim * NLAYERS + iLayer],
                  mPionPositiveFits[iDim * NLAYERS + iLayer]);
      }
    }
    printf("Finalization for %uD DONE!\n", nDim);
  }

  // Finalize all LL plots.
  // Meaning:
  //  1. Normalizing
  //  2. Calculate Efficiency
  void finalizeLL()
  {
    printf("Finalizing LL for %uD\n", nDim);
    Double_t integral;

    // Negative
    integral = mLLNegativeDistributions[0]->Integral();
    mLLNegativeDistributions[0]->Scale(1.0 / integral);
    integral = mLLNegativeDistributions[1]->Integral();
    mLLNegativeDistributions[1]->Scale(1.0 / integral);

    // Positive
    integral = mLLPositiveDistributions[0]->Integral();
    mLLPositiveDistributions[0]->Scale(1.0 / integral);
    integral = mLLPositiveDistributions[1]->Integral();
    mLLPositiveDistributions[1]->Scale(1.0 / integral);

    // Efficiency
    calcPionEfficiency();
  }

  // Fit a convolution of gaus*landau to a charge histogram.
  TF1* makeFit(TH1D* hist, int Qi)
  {
    // First we normalize the histogram.
    auto integral = hist->Integral("width");
    hist->Scale(1.0 / integral);

    double minCharge = hist->GetXaxis()->GetBinLowEdge(1);
    double maxCharge = hist->GetXaxis()->GetBinUpEdge(hist->GetNbinsX());
    double startFitMPV = hist->GetMean() / 1.00;
    double minFitValue = 0;
    double maxFitValue = 200;
    if (nDim == 3) {
      if (Qi == 0) {
        minFitValue = 13;
        maxFitValue = 100;
      } else if (Qi == 1) {
        minFitValue = 13;
        maxFitValue = 100;
      } else {
        minFitValue = 0;
        maxFitValue = 100;
      }
    }

    // Create convolution.
    auto name = TString::Format("%s_fit", hist->GetName());
    auto fConv = new TF1Convolution(
      "[3] * TMath::Landau(x, [0], [1]) * exp(-[2]*x)*[4]",
      "TMath::Gaus(x, 0, [0])", minCharge, maxCharge, true);
    fConv->SetNofPointsFFT(10'000);
    auto f = new TF1(name, *fConv, minCharge, maxCharge,
                     fConv->GetNpar());
    f->SetNpx(10'000);
    double p1, p2, p3, p5;
    if (nDim == 1) {
      f->SetParLimits(0, 30., 100.0);
      f->SetParLimits(1, 5.0, 25.0);
      f->SetParLimits(2, -0.1, 0.5);
      f->SetParLimits(3, -0.1, 0.5);
      f->SetParLimits(5, 1.0, 10.0);

      p1 = 15.;
      p2 = 0.02;
      p3 = 0.06;
      p5 = 3.0;
    retry1:
      f->SetParameter(0, startFitMPV);
      f->SetParameter(1, p1);
      f->SetParameter(2, p2);
      f->SetParameter(3, p3);
      f->FixParameter(4, 1.);
      f->SetParameter(5, p5);
    } else {
      if (Qi == 0) {
        f->SetParLimits(0, 0.1, 100.0);
        f->SetParLimits(1, 0.1, 10.0);
        f->SetParLimits(2, -1.0, 1.0);
        f->SetParLimits(3, 0.0, 1.0);
        f->SetParLimits(5, 1.0, 10.0);

        p1 = 5.;
        p2 = 0.2;
        p3 = 0.6;
        p5 = 1.5;
      } else if (Qi == 1) {
        f->SetParLimits(0, 0.1, 100.0);
        f->SetParLimits(1, 0.1, 10.0);
        f->SetParLimits(2, -1.0, 1.0);
        f->SetParLimits(3, 0.0, 1.0);
        f->SetParLimits(5, 1.0, 10.0);

        p1 = 5.;
        p2 = 0.2;
        p3 = 0.6;
        p5 = 1.5;
      } else {
        f->SetParLimits(0, 0.1, 100.0);
        f->SetParLimits(1, 0.1, 15.0);
        f->SetParLimits(2, -1.0, 1.0);
        f->SetParLimits(3, 0.0, 3.0);
        f->SetParLimits(5, -3.0, 3.0);

        p1 = 13.;
        p2 = 0.2;
        p3 = 0.6;
        p5 = 1.5;
      }
    retry3:
      f->SetParameter(0, startFitMPV);
      f->SetParameter(1, p1);
      f->SetParameter(2, p2);
      f->SetParameter(3, p3);
      f->FixParameter(4, 1.);
      f->SetParameter(5, p5);
    }

    // Fit it.
    if (hist->Fit(f, "QR0B", "", minFitValue, maxFitValue)) {
      std::cerr << "Fit failed for " << name << std::endl;
      static double min, max;
      static auto rand = new TRandom();
      f->GetParLimits(1, min, max);
      p1 = rand->Uniform(min, max);
      f->GetParLimits(2, min, max);
      p2 = rand->Uniform(min, max);
      f->GetParLimits(3, min, max);
      p3 = rand->Uniform(min, max);
      f->GetParLimits(5, min, max);
      p5 = rand->Uniform(min, max);
      std::cerr << "--------> Retrying with new Values are: p1=" << p1 << ",p2=" << p2 << ",p3=" << p3 << ",p5=" << std::endl;
      if constexpr (nDim == 1) {
        goto retry1;
      } else {
        goto retry3;
      }
    }

    integral = f->Integral(minCharge, maxCharge, 1.e-4);
    f->SetParameter(4, 1.f / integral);

    std::cout << "Successfully fitted " << name << std::endl;
    return f;
  }

  // Produce a LUT from the 2 Fits.
  // This is done per layer and per XD in finalize().
  // e.g. 1D->6 LUTs -> 1 Avg LUT.
  // 2D->12->2
  // 3D->18->3
  // NOTE: The likelihood calculations is here not dependend on XD.
  TGraph* makeLUT(TF1* eFit, TF1* rFit)
  {
    auto lut = new TGraph();

    lut->SetNameTitle(TString::Format("%s_lut", eFit->GetName()),
                      TString::Format("%s LUT", eFit->GetTitle()));
    lut->SetBit(TGraph::kIsSortedX);
    for (Double_t x = 0.0; x <= MAX_CHARGE; x += SPLINE_RES) {
      lut->AddPoint(x, eLikelihood(eFit->Eval(x), rFit->Eval(x)));
    }
    return lut;
  }

  // Draw some generic Plots.
  std::vector<TCanvas*> makeQNPlots()
  {
    int color = kBlack + 1;
    std::vector<TCanvas*> vec;
    // LUTs
    // Electrons
    auto electronLUTs = new TCanvas();
    electronLUTs->Divide(nDim, 1);
    electronLUTs->SetName(Form("electron_luts_%u_%s", nDim, mMom));
    electronLUTs->SetTitle("Electron LUTs");
    for (int iDim = 0; iDim < nDim; ++iDim) {
      color = kBlack + 1;
      electronLUTs->cd(iDim + 1);
      auto eMG = new TMultiGraph();
      eMG->GetXaxis()->SetTitle("charge (a.u.)");
      eMG->GetXaxis()->SetTitleSize(0.05);
      eMG->GetXaxis()->SetTitleOffset(0.8);
      eMG->GetYaxis()->SetTitle("L^{e}");
      eMG->GetYaxis()->SetTitleSize(0.05);
      eMG->GetYaxis()->SetTitleOffset(0.8);
      mElectronLUTsLow[iDim]->SetLineWidth(4);
      mElectronLUTsLow[iDim]->SetLineStyle(kDashed);
      mElectronLUTsLow[iDim]->SetLineColor(kCyan - 3);
      for (int i = 0; i < NLAYERS; ++i) {
        mElectronLUTs[i + iDim * NLAYERS]->SetLineColor(color++);
        mElectronLUTs[i + iDim * NLAYERS]->SetLineWidth(3);
        eMG->Add(mElectronLUTs[i + iDim * NLAYERS]);
      }
      eMG->Draw("al");
      mElectronLUTsLow[iDim]->Draw("same");

      if (nDim == 3) {
        if (iDim == 0 || iDim == 1) {
          eMG->GetXaxis()->SetRangeUser(0, 127);
        } else {
          eMG->GetXaxis()->SetRangeUser(0, 63);
        }
        eMG->SetTitle(Form("Q%d", iDim));
      } else {
        eMG->GetXaxis()->SetRangeUser(0, 317);
      }
      gPad->Modified();
      gPad->Update();
    }
    vec.push_back(electronLUTs);

    // Positron
    auto positronLUTs = new TCanvas();
    positronLUTs->Divide(nDim, 1);
    positronLUTs->SetName(Form("positron_luts_%u_%s", nDim, mMom));
    positronLUTs->SetTitle("Positron LUTs");
    for (int iDim = 0; iDim < nDim; ++iDim) {
      color = kBlack + 1;
      positronLUTs->cd(iDim + 1);
      auto pMG = new TMultiGraph();
      pMG->GetXaxis()->SetTitle("charge (a.u.)");
      pMG->GetXaxis()->SetTitleSize(0.05);
      pMG->GetXaxis()->SetTitleOffset(0.8);
      pMG->GetYaxis()->SetTitle("L^{e}");
      pMG->GetYaxis()->SetTitleSize(0.05);
      pMG->GetYaxis()->SetTitleOffset(0.8);
      mPositronLUTsLow[iDim]->SetLineWidth(4);
      mPositronLUTsLow[iDim]->SetLineStyle(kDashed);
      mPositronLUTsLow[iDim]->SetLineColor(kCyan - 3);
      for (int i = 0; i < NLAYERS; ++i) {
        mPositronLUTs[i + iDim * NLAYERS]->SetLineColor(color++);
        pMG->Add(mPositronLUTs[i + iDim * NLAYERS]);
      }
      mPositronLUTsLow[iDim]->Draw("same");
      pMG->Draw("al");
      if (nDim == 3) {
        if (iDim == 0 || iDim == 1) {
          pMG->GetXaxis()->SetRangeUser(0, 127);
        } else {
          pMG->GetXaxis()->SetRangeUser(0, 63);
        }
        pMG->SetTitle(Form("Q%d", iDim));
      } else {
        pMG->GetXaxis()->SetRangeUser(0, 317);
      }
      gPad->Modified();
      gPad->Update();
    }
    vec.push_back(positronLUTs);

    // // LL Distributions
    // // Electrons
    m90EffLineNeg->SetX1(m90EffNeg);
    m90EffLineNeg->SetX2(m90EffNeg);
    mLLNegativeDistributions[0]->SetLineColor(kBlue);
    mLLNegativeDistributions[1]->SetLineColor(kRed);
    auto llNegativeDistros = new TCanvas();
    llNegativeDistros->SetName(Form("ll_negative_distros_%u_%s", nDim, mMom));
    mLLNegativeStack->Draw("NOSTACK HIST");
    mLLNegativeStack->GetYaxis()->SetTitle("n. counts");
    mLLNegativeStack->GetYaxis()->SetTitleSize(0.05);
    mLLNegativeStack->GetYaxis()->SetTitleOffset(0.8);
    mLLNegativeStack->GetXaxis()->SetTitle("L^{e}");
    mLLNegativeStack->GetXaxis()->SetTitleSize(0.05);
    m90EffLineNeg->Draw("SAME");
    mLLNegativeLegend->Draw();
    gPad->SetLogy();
    vec.push_back(llNegativeDistros);

    // // Positrons
    m90EffLinePos->SetX1(m90EffPos);
    m90EffLinePos->SetX2(m90EffPos);
    mLLPositiveDistributions[0]->SetLineColor(kBlue);
    mLLPositiveDistributions[1]->SetLineColor(kRed);
    auto llPositiveDistros = new TCanvas();
    llPositiveDistros->SetName(Form("ll_positive_distros_%u_%s", nDim, mMom));
    mLLPositiveStack->Draw("NOSTACK HIST");
    mLLPositiveStack->GetYaxis()->SetTitle("n. counts");
    mLLPositiveStack->GetYaxis()->SetTitleSize(0.05);
    mLLPositiveStack->GetYaxis()->SetTitleOffset(0.8);
    mLLPositiveStack->GetXaxis()->SetTitle("L^{e}");
    mLLPositiveStack->GetXaxis()->SetTitleSize(0.05);
    m90EffLinePos->Draw("SAME");
    mLLPositiveLegend->Draw();
    gPad->SetLogy();
    vec.push_back(llPositiveDistros);

    // // Efficieny
    auto efficieny = new TCanvas();
    efficieny->SetName(Form("efficiency_%u_%s", nDim, mMom));
    efficieny->cd();
    mEfficiency->GetXaxis()->SetTitle("#epsilon_{e}");
    mEfficiency->GetYaxis()->SetTitle("#epsilon_{#pi}");
    mEfficiency->Draw("AP");
    mEffLegend->Draw();
    vec.push_back(efficieny);

    return vec;
  }

 protected:
  std::string const mMomS{random_string()};
  const char* mMom{mMomS.c_str()};
  double m90EffNeg{0.};
  double m90EffPos{0.};
  TLine* m90EffLineNeg;
  TLine* m90EffLinePos;
  std::array<TLine*, nDim> mElectronLUTsLow;
  std::array<TLine*, nDim> mPositronLUTsLow;
  std::array<TH1D*, nDim * NLAYERS>
    mElectronDistributions; // Charge Distributions per layer for Electrons
  std::array<TF1*, nDim * NLAYERS>
    mElectronFits; // Electron fits to charge distributions per layer
  std::array<TGraph*, nDim * NLAYERS>
    mElectronLUTs; // Electron likelihood per layer
  std::array<TH1D*, nDim * NLAYERS>
    mPositronDistributions; // Charge Distributions per layer for Positron
  std::array<TF1*, nDim * NLAYERS>
    mPositronFits; // Positron fits to charge distributions per layer
  std::array<TGraph*, nDim * NLAYERS>
    mPositronLUTs; // Positron likelihood per layer
  std::array<TH1D*, nDim * NLAYERS>
    mPionNegativeDistributions; // Charge Distributions per layer for
                                // PionNegative
  std::array<TF1*, nDim * NLAYERS>
    mPionNegativeFits; // PionNegative fits to charge distributions per layer
  std::array<TH1D*, nDim * NLAYERS>
    mPionPositiveDistributions; // Charge Distributions per layer for
                                // PionPositive
  std::array<TF1*, nDim * NLAYERS>
    mPionPositiveFits; // PionPositive fits to charge distributions per layer
  std::array<TH1D*, 2>
    mLLNegativeDistributions; // LL distributions (Electron + PionNegative)
  THStack* mLLNegativeStack;  // Stack for negative LL histograms
  TLegend* mLLNegativeLegend; // Legend for negative LL histograms
  std::array<TH1D*, 2>
    mLLPositiveDistributions;             // LL distributions (Position + PionPositive)
  THStack* mLLPositiveStack;              // Stack for positive LL histograms
  TLegend* mLLPositiveLegend;             // Legend for positive LL histograms
  std::array<TGraph*, 2> mPionEfficiency; // Graph electron efficiency vs.
                                          // resulting pion efficiency
  TMultiGraph* mEfficiency;               // Graph holding both pion efficiency graphs
  TLegend* mEffLegend;                    // Legend for efficiency graph

 private:
  // Calculate Likelihood
  Double_t eLikelihood(Double_t pe, Double_t pp)
  {
    double lh = pe / (pe + pp);

    // cut away impossible values
    lh = (lh >= 1.0) ? 1.0 : lh;
    lh = (lh < 0.0) ? 0.0 : lh;

    return lh;
  }

  // Calculate pion efficiency at electron efficiency [0.70-0.95;0.05]
  void calcPionEfficiency()
  {
    printf("Calculating Pion Efficiencies for %uD\n", nDim);
    constexpr std::array<double, 6> eff{0.70, 0.75, 0.80, 0.85, 0.90, 0.95};
    for (auto& e : eff) {
      double pEffNeg = 0.0;
      double pEffPos = 0.0;
      double teNeg = 0.0;
      double tePos = 0.0;
      int eBin = 0;
      int pBin = 0;

      // Negative
      for (eBin = mLLNegativeDistributions[0]->GetNbinsX(); eBin > 0; --eBin) {
        teNeg += mLLNegativeDistributions[0]->GetBinContent(eBin);
        if (teNeg >= e) {
          break;
        }
      }
      if (e == 0.9) {
        m90EffNeg = mLLNegativeDistributions[0]->GetBinCenter(eBin);
        printf("--------> 0.9 electron at %f\n", m90EffNeg);
      }
      printf("Neg: teNeg=%f, eBin=%d\n", teNeg, eBin);
      for (; eBin <= mLLNegativeDistributions[1]->GetNbinsX(); ++eBin) {
        pEffNeg += mLLNegativeDistributions[1]->GetBinContent(eBin);
      }

      // Positive
      for (pBin = mLLPositiveDistributions[0]->GetNbinsX(); pBin > 0; --pBin) {
        tePos += mLLPositiveDistributions[0]->GetBinContent(pBin);
        if (tePos >= e) {
          break;
        }
      }
      if (e == 0.9) {
        m90EffPos = mLLPositiveDistributions[0]->GetBinCenter(pBin);
        printf("--------> 0.9 positron at %f\n", m90EffPos);
      }
      printf("Pos: tePos=%f, pBin=%d\n", tePos, pBin);
      for (; pBin <= mLLPositiveDistributions[1]->GetNbinsX(); ++pBin) {
        pEffPos += mLLPositiveDistributions[1]->GetBinContent(pBin);
      }

      // Set Points in Graph
      mPionEfficiency[0]->AddPoint(e, pEffNeg);
      mPionEfficiency[1]->AddPoint(e, pEffPos);
      printf("%s -> %.2f-%f (%.2f)\n", mPionEfficiency[0]->GetName(), e,
             pEffNeg, 1.f / pEffNeg);
      printf("%s -> %.2f-%f (%.2f)\n", mPionEfficiency[1]->GetName(), e,
             pEffPos, 1.f / pEffPos);
    }
  }
};

// 1D-Likelihood method
class LQ1D : public LQND<1>
{
  using LQND<1>::LQND;

 public:
  LQ1D(int lowCutNegative = -1, int highCutNegative = -1,
       int lowCutPositive = -1, int highCutPositive = -1)
  {
    mElectronLUTsLow[0] = new TLine(lowCutNegative, 0.0, lowCutNegative, 0.9);
    mPositronLUTsLow[0] = new TLine(lowCutPositive, 0.0, lowCutPositive, 0.9);
    if (lowCutNegative >= 0) {
      mLowCutNegative = lowCutNegative;
    }
    if (highCutNegative >= 0) {
      mHighCutNegative = highCutNegative;
    }
    if (lowCutPositive >= 0) {
      mLowCutPositive = lowCutPositive;
    }
    if (highCutPositive >= 0) {
      mHighCutPositive = highCutPositive;
    }
    makeHistograms();
  }

  void addEntry(enum kType type, const Charges& charges, const uint8_t good) noexcept final
  {
    for (auto iLayer = 0; iLayer < NLAYERS; ++iLayer) {
      if (!(good & (1 << iLayer))) {
        continue;
      }
      auto q0 = charges[iLayer * NUM_CHARGES + 0];
      auto q1 = charges[iLayer * NUM_CHARGES + 1];
      auto q2 = charges[iLayer * NUM_CHARGES + 2];

      // In 1D we just add all charges together.
      auto q = q0 + q1 + q2;

      // add to hist and buffer
      switch (type) {
        case Electron:
          mElectronDistributions[iLayer]->Fill(q);
          break;
        case PionNegative:
          mPionNegativeDistributions[iLayer]->Fill(q);
          break;
        case Positron:
          mPositronDistributions[iLayer]->Fill(q);
          break;
        case PionPositive:
          mPionPositiveDistributions[iLayer]->Fill(q);
          break;
        default:
          std::cerr << "Unknown Type!" << std::endl;
      }
    }
  }

  void addLL(const enum kType type, const Charges& charges, const uint8_t good) noexcept final
  {
    double lei = 1.0;
    double lri = 1.0;

    // Loop through each layer and calculate common likelihood
    for (auto iLayer = 0; iLayer < NLAYERS; ++iLayer) {
      if (!(good & (1 << iLayer))) {
        continue;
      }
      auto q0 = charges[iLayer * NUM_CHARGES + 0];
      auto q1 = charges[iLayer * NUM_CHARGES + 1];
      auto q2 = charges[iLayer * NUM_CHARGES + 2];

      // In 1D we sum up
      auto q = q0 + q1 + q2;

      // if any combined charge is below the low cut off the
      // likelihood goes to zero immediatley hence we ignore those
      if (type == Electron && q < mLowCutNegative) {
        return;
      } else if (type == Positron && q < mLowCutPositive) {
        return;
      }

      auto ll = (type == Electron || type == PionNegative)
                  ? mElectronLUTs[iLayer]->Eval(q)
                  : mPositronLUTs[iLayer]->Eval(q);
      if (ll < 0. || ll > 1.) {
        return;
      }

      lei *= ll;
      lri *= (1.0 - ll);
    }
    if (lei == 1.0 && lri == 1.0)
      return;
    auto le = lei / (lei + lri);

    switch (type) {
      case Electron:
        mLLNegativeDistributions[0]->Fill(le);
        break;
      case PionNegative:
        mLLNegativeDistributions[1]->Fill(le);
        break;
      case Positron:
        mLLPositiveDistributions[0]->Fill(le);
        break;
      case PionPositive:
        mLLPositiveDistributions[1]->Fill(le);
        break;
      default:
        std::cerr << "Unknown Type!" << std::endl;
    }
  }

 private:
  int mHighCutNegative{MAX_CHARGE + 1}; // high cut value for negative particles
  int mLowCutNegative{0};               // loww cut value for negative particles
  int mHighCutPositive{MAX_CHARGE + 1}; // high cut value for positive particles
  int mLowCutPositive{0};               // loww cut value for positive particles
};

// 3D-Likelihood method
class LQ3D : public LQND<3>
{
  using LQND<3>::LQND;

 public:
  LQ3D(int lowCutNegativeQ0 = -1,
       int highCutNegativeQ0 = -1, int lowCutPositiveQ0 = -1,
       int highCutPositiveQ0 = -1, int lowCutNegativeQ1 = -1,
       int highCutNegativeQ1 = -1, int lowCutPositiveQ1 = -1,
       int highCutPositiveQ1 = -1, int lowCutNegativeQ2 = -1,
       int highCutNegativeQ2 = -1, int lowCutPositiveQ2 = -1,
       int highCutPositiveQ2 = -1)
  {
    mElectronLUTsLow[0] = new TLine(lowCutNegativeQ0, 0.0, lowCutNegativeQ0, 0.9);
    mPositronLUTsLow[0] = new TLine(lowCutPositiveQ0, 0.0, lowCutPositiveQ0, 0.9);
    mElectronLUTsLow[1] = new TLine(lowCutNegativeQ1, 0.0, lowCutNegativeQ1, 0.9);
    mPositronLUTsLow[1] = new TLine(lowCutPositiveQ1, 0.0, lowCutPositiveQ1, 0.9);
    mElectronLUTsLow[2] = new TLine(lowCutNegativeQ2, 0.0, lowCutNegativeQ2, 0.9);
    mPositronLUTsLow[2] = new TLine(lowCutPositiveQ2, 0.0, lowCutPositiveQ2, 0.9);
    if (lowCutNegativeQ0 >= 0) {
      mLowCutNegativeQ0 = lowCutNegativeQ0;
    }
    if (highCutNegativeQ0 >= 0) {
      mHighCutNegativeQ0 = highCutNegativeQ0;
    }
    if (lowCutPositiveQ0 >= 0) {
      mLowCutPositiveQ0 = lowCutPositiveQ0;
    }
    if (highCutPositiveQ0 >= 0) {
      mHighCutPositiveQ0 = highCutPositiveQ0;
    }
    if (lowCutNegativeQ1 >= 0) {
      mLowCutNegativeQ1 = lowCutNegativeQ1;
    }
    if (highCutNegativeQ1 >= 0) {
      mHighCutNegativeQ1 = highCutNegativeQ1;
    }
    if (lowCutPositiveQ1 >= 0) {
      mLowCutPositiveQ1 = lowCutPositiveQ1;
    }
    if (highCutPositiveQ1 >= 0) {
      mHighCutPositiveQ1 = highCutPositiveQ1;
    }
    if (lowCutNegativeQ2 >= 0) {
      mLowCutNegativeQ2 = lowCutNegativeQ2;
    }
    if (highCutNegativeQ2 >= 0) {
      mHighCutNegativeQ2 = highCutNegativeQ2;
    }
    if (lowCutPositiveQ2 >= 0) {
      mLowCutPositiveQ2 = lowCutPositiveQ2;
    }
    if (highCutPositiveQ2 >= 0) {
      mHighCutPositiveQ2 = highCutPositiveQ2;
    }
    makeHistograms();
  }

  void addEntry(enum kType type, const Charges& charges, const uint8_t good) noexcept final
  {
    for (auto iLayer = 0; iLayer < NLAYERS; ++iLayer) {
      if (!(good & (1 << iLayer))) {
        continue;
      }
      auto q0 = charges[iLayer * NUM_CHARGES + 0];
      auto q1 = charges[iLayer * NUM_CHARGES + 1];
      auto q2 = charges[iLayer * NUM_CHARGES + 2];

      // add to hist and buffer
      switch (type) {
        case Electron:
          mElectronDistributions[iLayer + NLAYERS * 0]->Fill(q0);
          mElectronDistributions[iLayer + NLAYERS * 1]->Fill(q1);
          mElectronDistributions[iLayer + NLAYERS * 2]->Fill(q2);
          break;
        case PionNegative:
          mPionNegativeDistributions[iLayer + NLAYERS * 0]->Fill(q0);
          mPionNegativeDistributions[iLayer + NLAYERS * 1]->Fill(q1);
          mPionNegativeDistributions[iLayer + NLAYERS * 2]->Fill(q2);
          break;
        case Positron:
          mPositronDistributions[iLayer + NLAYERS * 0]->Fill(q0);
          mPositronDistributions[iLayer + NLAYERS * 1]->Fill(q1);
          mPositronDistributions[iLayer + NLAYERS * 2]->Fill(q2);
          break;
        case PionPositive:
          mPionPositiveDistributions[iLayer + NLAYERS * 0]->Fill(q0);
          mPionPositiveDistributions[iLayer + NLAYERS * 1]->Fill(q1);
          mPionPositiveDistributions[iLayer + NLAYERS * 2]->Fill(q2);
          break;
        default:
          std::cerr << "Unknown Type!" << std::endl;
      }
    }
  }

  void addLL(const enum kType type, const Charges& charges, const uint8_t good) noexcept final
  {
    Double_t lei1 = 1.0;
    Double_t lei2 = 1.0;
    Double_t lei3 = 1.0;
    Double_t lri1 = 1.0;
    Double_t lri2 = 1.0;
    Double_t lri3 = 1.0;

    // Loop through each layer and calculate common likelihood
    for (auto iLayer = 0; iLayer < NLAYERS; ++iLayer) {
      if (!(good & (1 << iLayer))) {
        continue;
      }
      auto q0 = charges[iLayer * NUM_CHARGES + 0];
      auto q1 = charges[iLayer * NUM_CHARGES + 1];
      auto q2 = charges[iLayer * NUM_CHARGES + 2];

      if (type == Electron &&
          (q0 < mLowCutNegativeQ0 || q1 < mLowCutNegativeQ1 ||
           q2 < mLowCutNegativeQ2)) {
        return;
      } else if (type == Positron &&
                 (q0 < mLowCutPositiveQ0 || q1 < mLowCutPositiveQ1 ||
                  q2 < mLowCutPositiveQ2)) {
        return;
      }

      auto ll1 = (type == Electron || type == PionNegative)
                   ? mElectronLUTs[iLayer * 3 + 0]->Eval(q0)
                   : mPositronLUTs[iLayer * 3 + 0]->Eval(q0);
      auto ll2 = (type == Electron || type == PionNegative)
                   ? mElectronLUTs[iLayer * 3 + 1]->Eval(q1)
                   : mPositronLUTs[iLayer * 3 + 1]->Eval(q1);
      auto ll3 = (type == Electron || type == PionNegative)
                   ? mElectronLUTs[iLayer * 3 + 2]->Eval(q2)
                   : mPositronLUTs[iLayer * 3 + 2]->Eval(q2);
      if (ll1 < 0. || ll1 > 1. || ll2 < 0. || ll2 > 1. || ll3 < 0. ||
          ll3 > 1.) {
        return;
      }

      lei1 *= ll1;
      lei2 *= ll2;
      lei3 *= ll3;
      lri1 *= (1.0 - ll1);
      lri2 *= (1.0 - ll2);
      lri3 *= (1.0 - ll3);
    }
    if (lei1 == 1.0 && lei2 == 1.0 && lei3 == 1.0 && lri1 == 1.0 &&
        lri2 == 1.0 && lri3 == 1.0)
      return;
    auto le = lei1 * lei2 * lei3 / (lei1 * lei2 * lei3 + lri1 * lri2 * lei3);

    switch (type) {
      case Electron:
        mLLNegativeDistributions[0]->Fill(le);
        break;
      case PionNegative:
        mLLNegativeDistributions[1]->Fill(le);
        break;
      case Positron:
        mLLPositiveDistributions[0]->Fill(le);
        break;
      case PionPositive:
        mLLPositiveDistributions[1]->Fill(le);
        break;
      default:
        std::cerr << "Unknown Type!" << std::endl;
    }
  }

 private:
  int mHighCutNegativeQ0{MAX_CHARGE +
                         1}; // high cut value for negative particles Q0
  int mLowCutNegativeQ0{0};  // low cut value for negative particles Q0
  int mHighCutNegativeQ1{MAX_CHARGE +
                         1}; // high cut value for negative particles Q1
  int mLowCutNegativeQ1{0};  // low cut value for negative particles Q1
  int mHighCutNegativeQ2{MAX_CHARGE +
                         1}; // high cut value for negative particles Q2
  int mLowCutNegativeQ2{0};  // low cut value for negative particles Q2
  int mHighCutPositiveQ0{MAX_CHARGE +
                         1}; // high cut value for positive particles Q0
  int mLowCutPositiveQ0{0};  // low cut value for positive particles Q0
  int mHighCutPositiveQ1{MAX_CHARGE +
                         1}; // high cut value for positive particles Q1
  int mLowCutPositiveQ1{0};  // low cut value for positive particles Q1
  int mHighCutPositiveQ2{MAX_CHARGE +
                         1}; // high cut value for positive particles Q2
  int mLowCutPositiveQ2{0};  // low cut value for positive particles Q2
};

} // namespace o2::trd::pid

#endif // LXD_H_
