#include "LQND.h"

#include <memory>
#include <vector>

// ROOT header
#include "TROOT.h"
#include <Math/MinimizerOptions.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>

#define PBSTR "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
#define PBWIDTH 100

inline void printProgress(long long entries, double percentage)
{
  int val = (int)(percentage * 100);
  int lpad = (int)(percentage * PBWIDTH);
  int rpad = PBWIDTH - lpad;
  auto stop = std::chrono::high_resolution_clock::now();
  printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
  fflush(stdout);
}

void makeLikelihood(bool batch = true)
{
  using namespace o2::trd::pid;
  gStyle->SetOptStat(0);
  gROOT->SetBatch(batch);
  gROOT->ForceStyle();
  ROOT::Math::MinimizerOptions::SetDefaultTolerance(0.0001);
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
  ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(1'000'000);
  ROOT::Math::MinimizerOptions::SetDefaultMaxIterations(10'000);
  // ----------------------------------------------------
  // Input
  // ----------------------------------------------------
  std::unique_ptr<TFile> inFilePtr(TFile::Open("particleSkimmer.root"));
  if (!inFilePtr || inFilePtr->IsZombie()) {
    printf("Could not create input file!\n");
    return;
  }
  std::unique_ptr<TTree> inTreePtr(inFilePtr->Get<TTree>("skimmedData"));
  if (!inTreePtr) {
    printf("Could retrieve input tree!\n");
    return;
  }
  int sign;
  inTreePtr->SetBranchAddress("sign", &sign);
  int pid;
  inTreePtr->SetBranchAddress("pid", &pid);
  std::array<float, 6> pOut;
  inTreePtr->SetBranchAddress("momentum", &pOut);
  std::array<float, 18> chargeCor;
  inTreePtr->SetBranchAddress("charge", &chargeCor);
  uint8_t goodTracklets;
  inTreePtr->SetBranchAddress("goodTracklets", &goodTracklets);

  auto avgMom = [&]() {
    int c{0};
    float p{0};
    for (int iLayer = 0; iLayer < 6; ++iLayer) {
      if (!(goodTracklets & (1 << iLayer))) {
        continue;
      }
      p += pOut[iLayer];
      c++;
    }
    return p / c;
  };

  // ----------------------------------------------------
  // Vars
  // ----------------------------------------------------
  LQ1D l1P1(30, -1, 25);
  LQ3D l3P1(4, -1, 4, -1, 7, -1, 14, -1, 10, -1, 10, -1);
  LQ1D l1P2(35, -1, 32);
  LQ3D l3P2(2, -1, 2, -1, 17, -1, 16, -1, 10, -1, 10, -1);
  LQ1D l1P3(35, -1, 35, -1);
  LQ3D l3P3(3, -1, 3, -1, 20, -1, 20, -1, 11, -1, 10, -1);
  LQ1D l1P4(15, -1, 15, -1);
  LQ3D l3P4(3, -1, 6, -1, 20, -1, 21, -1, 10, -1, 10, -1);

  auto type{kType::Electron};
  long long nEntries = inTreePtr->GetEntriesFast();
  // ----------------------------------------------------
  // Loop
  // ----------------------------------------------------
  std::cout << "Building LUT" << std::endl;
  for (Int_t iEntry = 0; inTreePtr->LoadTree(iEntry) >= 0; ++iEntry) {
    inTreePtr->GetEntry(iEntry);
    if (pid == 0) {
      continue;
    }
    auto p = avgMom();
    if (sign == -1) {
      if (pid == 1) {
        type = kType::Electron;
      } else {
        type = kType::PionNegative;
      }
    } else {
      if (pid == 1) {
        type = kType::Positron;
      } else {
        type = kType::PionPositive;
      }
    }

    if (p < 1.0) {
      l1P1.addEntry(type, chargeCor, goodTracklets);
      l3P1.addEntry(type, chargeCor, goodTracklets);
    } else if (1.0 < p && p < 2.0) {
      l1P2.addEntry(type, chargeCor, goodTracklets);
      l3P2.addEntry(type, chargeCor, goodTracklets);
    } else if (2.0 < p && p < 3.0) {
      l1P3.addEntry(type, chargeCor, goodTracklets);
      l3P3.addEntry(type, chargeCor, goodTracklets);
    } else if (3.0 < p && p < 8.0) {
      l1P4.addEntry(type, chargeCor, goodTracklets);
      l3P4.addEntry(type, chargeCor, goodTracklets);
    }
    printProgress(nEntries, (double)iEntry / (double)nEntries);
  }

  // ----------------------------------------------------
  // Finalize Entries
  // ----------------------------------------------------
  l1P1.finalizeEntries();
  l3P1.finalizeEntries();
  l1P2.finalizeEntries();
  l3P2.finalizeEntries();
  l1P3.finalizeEntries();
  l3P3.finalizeEntries();
  l1P4.finalizeEntries();
  l3P4.finalizeEntries();

  std::cout << "Making likelihood plots" << std::endl;
  for (Int_t iEntry = 0; inTreePtr->LoadTree(iEntry) >= 0; ++iEntry) {
    inTreePtr->GetEntry(iEntry);
    if (pid == 0) {
      continue;
    }
    auto p = avgMom();
    if (sign == -1) {
      if (pid == 1) {
        type = kType::Electron;
      } else {
        type = kType::PionNegative;
      }
    } else {
      if (pid == 1) {
        type = kType::Positron;
      } else {
        type = kType::PionPositive;
      }
    }

    if (p < 1.0) {
      l1P1.addLL(type, chargeCor, goodTracklets);
      l3P1.addLL(type, chargeCor, goodTracklets);
    } else if (1.0 < p && p < 2.0) {
      l1P2.addLL(type, chargeCor, goodTracklets);
      l3P2.addLL(type, chargeCor, goodTracklets);
    } else if (2.0 < p && p < 3.0) {
      l1P3.addLL(type, chargeCor, goodTracklets);
      l3P3.addLL(type, chargeCor, goodTracklets);
    } else if (3.0 < p && p < 8.0) {
      l1P4.addLL(type, chargeCor, goodTracklets);
      l3P4.addLL(type, chargeCor, goodTracklets);
    }
    printProgress(nEntries, (double)iEntry / (double)nEntries);
  }

  // // ----------------------------------------------------
  // // Finalize LL
  // // ----------------------------------------------------
  l1P1.finalizeLL();
  l3P1.finalizeLL();
  l1P2.finalizeLL();
  l3P2.finalizeLL();
  l1P3.finalizeLL();
  l3P3.finalizeLL();
  l1P4.finalizeLL();
  l3P4.finalizeLL();

  // // ----------------------------------------------------
  // // Write
  // // ----------------------------------------------------
  std::cout << "Writeout" << std::endl;
  std::unique_ptr<TFile> file(TFile::Open("makeLikelihood.root", "RECREATE"));
  file->mkdir("LQ1D_P1");
  file->cd("LQ1D_P1");
  auto c1P1 = l1P1.makeQNPlots();
  for (auto& c : c1P1)
    c->Write();
  file->cd("..");
  file->mkdir("LQ3D_P1");
  file->cd("LQ3D_P1");
  auto c3P1 = l3P1.makeQNPlots();
  for (auto& c : c3P1)
    c->Write();
  file->cd("..");

  file->mkdir("LQ1D_P2");
  file->cd("LQ1D_P2");
  auto c1P2 = l1P2.makeQNPlots();
  for (auto& c : c1P2)
    c->Write();
  file->cd("..");
  file->mkdir("LQ3D_P2");
  file->cd("LQ3D_P2");
  auto c3P2 = l3P2.makeQNPlots();
  for (auto& c : c3P2)
    c->Write();
  file->cd("..");

  file->mkdir("LQ1D_P3");
  file->cd("LQ1D_P3");
  auto c1P3 = l1P3.makeQNPlots();
  for (auto& c : c1P3)
    c->Write();
  file->cd("..");
  file->mkdir("LQ3D_P3");
  file->cd("LQ3D_P3");
  auto c3P3 = l3P3.makeQNPlots();
  for (auto& c : c3P3)
    c->Write();
  file->cd("..");

  file->mkdir("LQ1D_P4");
  file->cd("LQ1D_P4");
  auto c1P4 = l1P4.makeQNPlots();
  for (auto& c : c1P4)
    c->Write();
  file->cd("..");
  file->mkdir("LQ3D_P4");
  file->cd("LQ3D_P4");
  auto c3P4 = l3P4.makeQNPlots();
  for (auto& c : c3P4)
    c->Write();
  file->cd("..");
}
