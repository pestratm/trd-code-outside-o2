#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.stats import moyal
from scipy.signal import convolve, deconvolve

def gauss(x, mu, sigma):
    return 1/np.sqrt(2*np.pi*sigma**2)*np.exp(-(x-mu)**2/sigma**2)

def norm(array, A):
    return array * A/np.sum(array)

"""
def shorten(array, n):
    #new_array = np.zeros(n)
    #r = len(array)/n
    #for i in range(n):
    #    new_array[i] = np.mean(array[int(r*i):int(r*(i+1))])
    index_array = np.array(range(0, len(array), int(len(array)/n)))
    new_array = array[index_array]
    return new_array
"""

def convolve_php(unconvolved_php, tb_php, trf, tb_trf, n_tb="php", normalize=True):
    """
    Parameters
    ----------
    unconvolved_php : 1-dim array
        unconvolved pulse-height plot.
    tb_php : 1-dim arra
        time bins of pulse-height plot.
    trf : 1-dim arra
        time response function to convolute with.
    tb_trf : 1-dim arra
        time bins of trf.
    n_tb : str, optional
        Which time-bin width should be used (php or trf). The default is "php".
    normalize : bool, optional
        Whether the pulse-height plot should be normalized. The default is True.

    Returns
    -------
    tb_convolved : 1-dim array
        time bins of convolved pulse-height plot.
    convolved_php : 1-dim array
        convolved pulse-height plot.

    """
    if(n_tb=="php"):
        shortened_tb_trf = np.arange(tb_trf[0], tb_trf[-1], tb_php[1]-tb_php[0])
        shortened_trf = np.interp(shortened_tb_trf, tb_trf, trf)
        convolved_php = convolve(unconvolved_php, shortened_trf)
        tb_convolved = np.linspace(tb_php[0]+shortened_tb_trf[0],
                                   tb_php[-1]+shortened_tb_trf[-1], 
                                   len(tb_php)+len(shortened_tb_trf)-1)
        if normalize: convolved_php = norm(convolved_php, np.sum(unconvolved_php))
    if(n_tb=="trf"):
        interp_php = np.interp(tb_trf, tb_php, unconvolved_php)
        convolved_php = convolve(interp_php, trf)  
        tb_convolved = np.linspace(2*tb_trf[0], 2*tb_trf[-1], 2*len(tb_trf)-1)
        if normalize: convolved_php = norm(convolved_php, np.sum(unconvolved_php)) * len(tb_convolved) / len(tb_php)
    return tb_convolved, convolved_php

def autocorr(hist):
    """
    Parameters
    ----------
    hist : 1-dim array
        histogram of which autocorrelation should be calculated

    Returns
    -------
    ac : 1-dim array
        autocorrelation of histogram

    """
    ac = np.zeros_like(hist)
    mean = np.mean(hist)
    var  = np.sum((hist-mean)**2)
    T = len(hist)
    for k in range(0, T):
        val = 0
        for t in range(k, T):
            val += (hist[t] - mean)*(hist[t-k] - mean)
        val /= var
        ac[k] = val
    return ac
    
#Pulse-height plot
tb = np.linspace(0., 2.9, 30) #Time bins

#Real pulse-height plot
ph_real = np.array([106.312, 186.028, 165.812, 142.324, 136.276, 
                    137.99, 137.059, 138.601, 142.172, 141.376, 
                    142.482, 145.621, 144.085, 144.569, 146.779, 
                    144.607, 144.01, 145.284, 141.387, 138.699, 
                    136.651, 127.864, 116.772, 104.283, 88.428, 
                    79.0391, 75.0675, 68.1009, 64.2372, 62.9963])

#Simulated pulse-height plot using default TRF
ph_sim = np.array([82.5414, 166.4, 177.32, 170.103, 168.782, 
                   171.039, 175.227, 180.386, 185.32, 189.416, 
                   192.913, 196.714, 199.894, 203.637, 206.262, 
                   205.906, 204.558, 202.897, 200.453, 196.665, 
                   189.567, 173.387, 148.546, 126.581, 111.213, 
                   100.465, 92.6328, 86.4739, 81.1411, 76.3973])
"""
         np.array([73.8121, 134.333, 139.636, 134.696, 133.872, 135.765, 
                   140.081, 144.415, 148.179, 151.81, 155.712, 159.39, 162.395, 
                   166.172, 169.033, 169.253, 168.539, 167.927, 166.924, 
                   165.157, 160.193, 146.453, 126.625, 109.836, 97.9414, 
                   89.4346, 83.1603, 78.023, 73.3662, 69.0684])
"""

#Simulated pulse-height plot without TRF
ph_sim_unconv = np.array([  7.92123288, 127.55958904,  43.94760274,  
                           45.74965753, 46.95068493,  45.25993151,  
                           44.67842466,  44.58732877, 43.90239726,  
                           43.46130137,  43.88047945,  46.27363014,
                           46.21267123,  47.82534247,  41.71678082,  
                           41.30136986, 41.125     ,  38.82739726,  
                           38.60650685,  38.83184932, 36.70547945,  
                           30.0859589,  14.10547945,  12.34109589,
                           11.21849315,  10.19178082,   9.40068493,   
                           9.04143836, 9.05410959,   8.97773973])
"""
                np.array([16.1352, 134.623, 46.7003, 51.3367, 58.544, 54.5116, 
                          57.4154, 59.8813, 60.0899, 63.9896, 62.3545, 64.1631, 
                          64.7107, 63.3782, 63.1362, 61.0797, 61.7963, 58.2536, 
                          55.7855, 51.703, 47.9801, 40.8399, 29.4725, 25.0795, 
                          22.1043, 21.2819, 20.2468, 19.8221, 19.5912, 18.5698])
"""

#Simulated pulse-height plot using TDR TRF
ph_sim_tdr = np.array([45.4617, 119.735, 111.438, 103.405, 103.77, 104.56, 
                       106.588, 109.151, 111.615, 113.893, 116.168, 118.287, 
                       120.01, 122.568, 125.003, 124.351, 123.334, 122.327, 
                       120.893, 118.847, 115.083, 104.674, 87.4739, 74.5765, 
                       66.5337, 60.9541, 57.0355, 53.9426, 51.5429, 49.5324])
"""
             np.array([ 45.5911, 122.247 , 113.17  , 104.414 , 104.89  , 106.417 ,
                       108.536 , 111.484 , 114.177 , 115.889 , 117.937 , 119.601 ,
                       120.871 , 123.659 , 125.851 , 124.543 , 123.422 , 122.063 ,
                       119.948 , 117.673 , 114.203 , 103.927 ,  86.6463,  73.8055,
                       65.6706,  60.1483,  56.3043,  53.4205,  51.0821,  49.0741])
"""




#Time Response Function
tb_trf_short = np.linspace(-.4, 3.5, 40)

tb_trf_default = np.linspace(-.4, 3.58, 200)
trf_default = np.array([0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0002, 0.0007, 0.0026, 0.0089, 0.0253, 0.0612, 0.1319,
                0.2416, 0.3913, 0.5609, 0.7295, 0.8662, 0.9581, 1.0000, 0.9990, 0.9611, 0.8995, 0.8269, 0.7495, 0.6714, 0.5987,
                0.5334, 0.4756, 0.4249, 0.3811, 0.3433, 0.3110, 0.2837, 0.2607, 0.2409, 0.2243, 0.2099, 0.1974, 0.1868, 0.1776,
                0.1695, 0.1627, 0.1566, 0.1509, 0.1457, 0.1407, 0.1362, 0.1317, 0.1274, 0.1233, 0.1196, 0.1162, 0.1131, 0.1102,
                0.1075, 0.1051, 0.1026, 0.1004, 0.0979, 0.0956, 0.0934, 0.0912, 0.0892, 0.0875, 0.0858, 0.0843, 0.0829, 0.0815,
                0.0799, 0.0786, 0.0772, 0.0757, 0.0741, 0.0729, 0.0718, 0.0706, 0.0692, 0.0680, 0.0669, 0.0655, 0.0643, 0.0630,
                0.0618, 0.0607, 0.0596, 0.0587, 0.0576, 0.0568, 0.0558, 0.0550, 0.0541, 0.0531, 0.0522, 0.0513, 0.0505, 0.0497,
                0.0490, 0.0484, 0.0474, 0.0465, 0.0457, 0.0449, 0.0441, 0.0433, 0.0425, 0.0417, 0.0410, 0.0402, 0.0395, 0.0388,
                0.0381, 0.0374, 0.0368, 0.0361, 0.0354, 0.0348, 0.0342, 0.0336, 0.0330, 0.0324, 0.0318, 0.0312, 0.0306, 0.0301,
                0.0296, 0.0290, 0.0285, 0.0280, 0.0275, 0.0270, 0.0265, 0.0260, 0.0256, 0.0251, 0.0246, 0.0242, 0.0238, 0.0233,
                0.0229, 0.0225, 0.0221, 0.0217, 0.0213, 0.0209, 0.0206, 0.0202, 0.0198, 0.0195, 0.0191, 0.0188, 0.0184, 0.0181,
                0.0178, 0.0175, 0.0171, 0.0168, 0.0165, 0.0162, 0.0159, 0.0157, 0.0154, 0.0151, 0.0148, 0.0146, 0.0143, 0.0140,
                0.0138, 0.0135, 0.0133, 0.0131, 0.0128, 0.0126, 0.0124, 0.0121, 0.0119, 0.0120, 0.0115, 0.0113, 0.0111, 0.0109,
                0.0107, 0.0105, 0.0103, 0.0101, 0.0100, 0.0098, 0.0096, 0.0094, 0.0092, 0.0091, 0.0089, 0.0088, 0.0086, 0.0084,
                0.0083, 0.0081, 0.0080, 0.0078])

tb_raphaelle = np.linspace(.2, 3.1, 30)
trf_raphaelle = np.array([0.0589726 , 0.72616438, 0.95947489, 0.59751712, 0.34006849,
       0.2234589 , 0.16708724, 0.14033038, 0.1204501 , 0.10410959,
       0.09318493, 0.08356164, 0.07656343, 0.07074364, 0.06285254,
       0.0572407 , 0.05342466, 0.04863014, 0.04302981, 0.03943249,
       0.03565068, 0.03219178, 0.03020548, 0.02681018, 0.02291886,
       0.02215605, 0.01989906, 0.01776793, 0.01536204, 0.01500893])

"""
tb_tdr = np.linspace(0, 1.99, 200)
trf_tdr = np.array([  0.88852989,   0.44426494,   0.44426494,   0.44426494,
         0.44426494,   0.44426494,   0.44426494,   0.44426494,
         0.44426494,   0.44426494,   0.44426494,   0.44426494,
         0.44426494,   0.44426494,   0.44426494,   0.44426494,
         0.44426494,   1.33279483,   5.33117932,  19.54765751,
        38.65105008,  67.97253635, 109.73344103, 142.60904685,
       178.15024233, 210.13731826, 235.01615509, 251.89822294,
       261.22778675, 265.67043619, 263.89337641, 257.67366721,
       247.45557351, 235.46042003, 222.57673667, 205.69466882,
       192.81098546, 178.15024233, 164.82229402, 151.05008078,
       139.0549273 , 128.8368336 , 116.84168013, 107.51211632,
       100.40387722,  91.51857835,  84.8546042 ,  79.52342488,
        74.19224556,  69.30533118,  64.86268174,  60.86429725,
        57.75444265,  54.64458805,  51.97899838,  49.31340872,
        47.09208401,  45.31502423,  43.09369952,  41.76090468,
        39.98384491,  38.65105008,  36.87399031,  35.98546042,
        35.09693053,  33.7641357 ,  32.87560582,  31.98707593,
        31.09854604,  29.76575121,  29.32148627,  28.43295638,
        27.54442649,  27.10016155,  26.65589661,  25.76736672,
        25.32310178,  24.43457189,  24.43457189,  23.546042  ,
        23.10177706,  22.65751212,  22.21324717,  21.76898223,
        21.32471729,  20.88045234,  20.4361874 ,  19.99192246,
        19.54765751,  19.10339257,  19.10339257,  18.65912763,
        18.21486268,  17.77059774,  17.32633279,  17.32633279,
        16.88206785,  16.88206785,  16.43780291,  16.43780291,
        15.99353796,  15.54927302,  15.54927302,  15.10500808,
        14.66074313,  14.66074313,  14.21647819,  14.21647819,
        14.21647819,  13.77221325,  13.77221325,  13.3279483 ,
        13.3279483 ,  12.88368336,  12.88368336,  12.43941842,
        12.43941842,  11.99515347,  11.99515347,  11.55088853,
        11.55088853,  11.55088853,  11.55088853,  11.55088853,
        11.10662359,  11.10662359,  11.10662359,  10.66235864,
        10.66235864,  10.2180937 ,  10.2180937 ,  10.2180937 ,
         9.77382876,   9.77382876,   9.77382876,   9.32956381,
         9.32956381,   9.32956381,   9.32956381,   8.88529887,
         9.32956381,   8.88529887,   8.88529887,   8.88529887,
         8.88529887,   8.44103393,   8.44103393,   8.44103393,
         8.44103393,   8.44103393,   8.44103393,   7.99676898,
         7.99676898,   7.99676898,   7.99676898,   7.55250404,
         7.55250404,   7.55250404,   7.55250404,   7.55250404,
         7.1082391 ,   7.1082391 ,   7.1082391 ,   7.1082391 ,
         7.1082391 ,   6.66397415,   6.66397415,   6.66397415,
         6.66397415,   7.1082391 ,   6.66397415,   6.66397415,
         6.66397415,   6.21970921,   6.66397415,   6.21970921,
         6.21970921,   6.21970921,   6.21970921,   6.21970921,
         6.21970921,   6.21970921,   6.21970921,   6.21970921,
         5.77544426,   5.77544426,   5.77544426,   5.77544426,
         5.77544426,   5.77544426,   5.77544426,   5.77544426,
         5.33117932,   5.33117932,   5.33117932,   5.33117932,
         5.33117932,   5.33117932,   5.33117932,   5.33117932])
tb_tdr -= tb_tdr[np.argmax(trf_tdr)] #So that maximum is at t=0
"""

tb_tdr = tb_trf_default
trf_tdr = np.array([0.00334448, 0.00334448, 0.00334448, 0.00334448, 0.00334448,
       0.00334448, 0.00167224, 0.00167224, 0.00167224, 0.00167224,
       0.00167224, 0.00167224, 0.00167224, 0.00167224, 0.00501672,
       0.0735786, 0.25585284, 0.5367893, 0.7909699, 0.94816054,
       1., 0.96989967, 0.88628763, 0.77424749, 0.67056856,
       0.56856187, 0.48494983, 0.40468227, 0.34448161, 0.2993311,
       0.26086957, 0.22909699, 0.20568562, 0.18561873, 0.17056856,
       0.15719064, 0.14548495, 0.13545151, 0.1270903, 0.12040134,
       0.11204013, 0.10702341, 0.10200669, 0.09698997, 0.09197324,
       0.08862876, 0.08528428, 0.0819398, 0.07859532, 0.07525084,
       0.07190635, 0.07023411, 0.06688963, 0.06521739, 0.06354515,
       0.06187291, 0.05852843, 0.05685619, 0.05518395, 0.05351171,
       0.05183946, 0.05016722, 0.04849498, 0.04682274, 0.0451505,
       0.04347826, 0.04347826, 0.04347826, 0.04180602, 0.04013378,
       0.03846154, 0.03846154, 0.0367893, 0.03511706, 0.03511706,
       0.03344482, 0.03344482, 0.03344482, 0.03177258, 0.03177258,
       0.03177258, 0.03010033, 0.03010033, 0.02842809, 0.02842809,
       0.02842809, 0.02675585, 0.02675585, 0.02508361, 0.02508361,
       0.02675585, 0.02508361, 0.02341137, 0.02341137, 0.02341137,
       0.02341137, 0.02341137, 0.02341137, 0.02173913, 0.02173913,
       0.02173913, 0.02173913, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689,
       0.02006689, 0.02006689, 0.02006689, 0.02006689, 0.02006689])

landau1 = moyal.pdf(tb_trf_default, 0., 0.03)
landau2 = moyal.pdf(tb_trf_default, 0., 0.045)

#Compare effect of convolution
plt.plot(tb, norm(ph_sim_unconv,1 ), '-', label='Simulated with no TRF')
plt.plot(tb, norm(ph_sim, 1), '-', color='orange', label='Simulated with default TRF')
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_default, tb_trf_default)
plt.plot(tb_conv, norm(php_conv, 1), '--', color='orange', label='Conv with default TRF')
#plt.plot(tb, norm(ph_sim_tdr, 1), '-', color='green', label='Simulated with TDR TRF')
#tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_tdr, tb_tdr)
#plt.plot(tb_conv, norm(php_conv, 1), '--', color='green', label='Conv with TDR TRF')
plt.xlabel('Time [us]')
plt.ylabel('ADC (normalized)')
plt.xlim([0,3])
plt.ylim([0, 0.08])
plt.legend(loc=1)
plt.show()

#Show the effect of the peak and the plateau
ph_sim_unconv_peak = np.zeros_like(ph_sim_unconv) #Just the peak
ph_sim_unconv_peak[:3] = ph_sim_unconv[:3]
ph_sim_unconv_plateau = np.zeros_like(ph_sim_unconv) #Just the plateau
ph_sim_unconv_plateau[3:] = ph_sim_unconv[3:]
tb_conv, php_conv = convolve_php(ph_sim_unconv_peak, tb, trf_default, tb_trf_default)
plt.plot(tb_conv, php_conv, label='Amplification peak')
tb_conv, php_conv = convolve_php(ph_sim_unconv_plateau, tb, trf_default, tb_trf_default)
plt.plot(tb_conv, php_conv, label='Drift region')
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_default, tb_trf_default)
plt.plot(tb_conv, php_conv, label='Full')
plt.xlabel('Time [us]')
plt.ylabel('ADC')
plt.xlim([0,3])
plt.legend(loc=4)
plt.show()

#Show effects of different TRFs
plt.plot(tb_trf_default, trf_default/np.max(trf_default), label = 'Default TRF')
plt.plot(tb_tdr, trf_tdr/np.max(trf_tdr), label='TDR TRF')
plt.plot(tb_trf_default, landau1/np.max(landau1), label="Landau TRF, $ \sigma $ =0.03us")
plt.plot(tb_trf_default, landau2/np.max(landau2), label="Landau TRF, $ \sigma $ =0.045us")
plt.xlabel('Time [us]')
plt.ylabel('TRF')
plt.xlim([-0.5, 2.])
plt.legend(loc=1)
plt.show()

tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_default, tb_trf_default)
plt.plot(tb_conv, norm(php_conv, 1), label='Default TRF')
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_tdr, tb_tdr)
plt.plot(tb_conv, norm(php_conv, 1), label='TDR TRF')
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, landau1, tb_trf_default)
plt.plot(tb_conv, norm(php_conv, 1), label="Landau TRF, $ \sigma $ =0.03us")
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, landau2, tb_trf_default)
plt.plot(tb_conv, norm(php_conv, 1), label="Landau TRF, $ \sigma $ =0.045us")
plt.plot(tb, norm(ph_real,1), label="Data")
plt.xlabel('Time [us]')
plt.ylabel('ADC')
plt.xlim([0., 3.])
plt.legend(loc=1)
plt.show()

#Show autocorrelations
plt.plot(tb, autocorr(ph_real), label="Data")
plt.plot(tb, autocorr(ph_sim_unconv), label="Sim, no TRF")
plt.plot(tb, autocorr(ph_sim), label="Sim, default TRF")
plt.plot(tb, autocorr(ph_sim_tdr), label="Sim, TDR TRF")
plt.xlabel('Time [us]')
plt.ylabel('Autocorrelation')
plt.xlim([0., 3.])
plt.legend(loc=1)
plt.show()

plt.plot(tb, autocorr(ph_sim), color="orange", label="Default TRF, O2 sim")
tb_conv, php_conv = convolve_ph# TRD Simulation Documentation

## Creating Pulse-Height Plots

CreatePulseHeightPlot is a macro to create pulse-height plots using (real or simulated) TRD digits. As input files it takes:

- ```o2sim_geometry-aligned.root```
- ```o2sim_grp.root```
- ```trddigits.root```
- ```trdtracklets.root```
- ```o2_tfidinfo.root```

The macro uses the DataManager.C to read out the digit data, where it loops over all time frames and events. The digits are clustered into Continuous Regions, that are regions of digits in the same detector and pad row, and adjacent pad columns. From these Continuous Regions it finds Regions of Interest (RoI) that are said to constitute a signal. The Region of Interest contain the digit in the continuous region with the highest ADC sum value, its neighbouring pad with the highest ADC sum value, and the two pads adjacent to them. Quality cuts are applied to filter out low quality signals. From these Regions of Interest, the pulse-height plot is calculated, by adding and later averaging the ADC values in each time bin.
CreatePulseHeightPlot.C creates a file pulseheight.root that contains:

- ```pulse-height```, a 1-dim histogram containing the pulse-height plot
- ```tb_adc```, a 2-dim histogram showing the distribution of ADC values in each time bin
- ```ADC```, a directory that contains histograms showing the distribution of ADC values (such as the values themselves, the sum in each digit, etc.)
- ```Signals```, a directory containg 2-dim histograms of the signals extracted
- ```Signals_1d```, a directory containg the 1-dim projections of the histograms in Signals

As a further check, digits can be excluded when they are matched to (TPC or ITS-TPC) tracks that have pad row crossings. In this case, the filed ```trdmatches_itstpc.root``` and/or ```trdmatches_tpc.root``` must be provided, and instead of the DataManager.C macro, the DataManagerTracks.C macro is used. 
To create the input files, the O2 TRD [simulation](https://github.com/AliceO2Group/AliceO2/tree/dev/Detectors/TRD/simulation) and [reconstruction](https://github.com/AliceO2Group/AliceO2/tree/dev/Detectors/TRD/workflow) workflow is used. An examplary workflow could be for MC:

```bash
o2-sim -m PIPE MAG TRD -n 5000 -g boxgen --configKeyValues 'BoxGun.pdg=211;BoxGun.eta[0]=-0.84;BoxGun.eta[1]=0.84;BoxGun.prange[0]=1.;BoxGun.prange[1]=5.'
o2-sim-digitizer-workflow --configKeyValues 'TRDSimParams.trf=1'
o2-tpc-reco-workflow --input-type digits --output-type clusters,tracks 
o2-its-reco-workflow --trackerCA --tracking-mode async
o2-tpcits-match-workflow --tpc-track-reader tpctracks.root --tpc-native-cluster-reader "--infile tpc-native-clusters.root"
o2-trd-tracklet-transformer --filter-trigrec
o2-trd-global-tracking --filter-trigrec
```

where only the first two tasks are needed if no matching to ITS/ITS-TPC is done.

## Time-Response Function Convolutions


p(ph_sim_unconv, tb, trf_default, tb_trf_default, n_tb='trf')
tb_conv, php_conv = tb_conv[40:190], php_conv[40:190]
plt.plot(tb_conv, autocorr(php_conv), '--', color="orange", label="Default TRF, Python conv, php time bins")
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_default, tb_trf_default)
tb_conv, php_conv = tb_conv[4:34], php_conv[4:34]
plt.plot(tb_conv, autocorr(php_conv), '-.', color="orange", label="Default TRF, Python conv, trf time bins")

plt.plot(tb, autocorr(ph_sim), color="green", label="TDR TRF, O2 sim")
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_tdr, tb_tdr, n_tb='trf')
tb_conv, php_conv = tb_conv[40:190], php_conv[40:190]
plt.plot(tb_conv, autocorr(php_conv), '--', color="green", label="TDR TRF, Python conv, php time bins")
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_tdr, tb_tdr)
tb_conv, php_conv = tb_conv[4:34], php_conv[4:34]
plt.plot(tb_conv, autocorr(php_conv), '-.', color="green", label="TDR TRF, Python conv, trf time bins")

plt.xlabel('Time [us]')
plt.ylabel('Autocorrelation')
plt.xlim([0., 3.])
plt.legend(loc=1)
plt.show()

#Show for time bins to 6
plt.plot(tb, autocorr(ph_real), label="Data")
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_default, tb_trf_default, n_tb='trf')
tb_conv, php_conv = tb_conv[40:190], php_conv[40:190]
plt.plot(tb_conv, autocorr(php_conv), label="Default TRF")
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, trf_tdr, tb_tdr, n_tb='trf')
tb_conv, php_conv = tb_conv[40:190], php_conv[40:190]
plt.plot(tb_conv, autocorr(php_conv), label="TDR TRF")
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, landau1, tb_trf_default, n_tb='trf')
tb_conv, php_conv = tb_conv[40:190], php_conv[40:190]
plt.plot(tb_conv, autocorr(php_conv), label="Landau TRF, $ \sigma $ =0.03us")
tb_conv, php_conv = convolve_php(ph_sim_unconv, tb, landau2, tb_trf_default, n_tb='trf')
tb_conv, php_conv = tb_conv[40:190], php_conv[40:190]
plt.plot(tb_conv, autocorr(php_conv), label="Landau TRF, $ \sigma $ =0.045us")
plt.xlabel('Time [us]')
plt.ylabel('Autocorrelation')
plt.xlim([0., .6])
plt.ylim([0., 1.1])
plt.legend(loc=1)
plt.show()
