#include "./macros/DataManager.C"
//#include "./macros/DataManagerTracks.C" //Needed for track matching
#include <iostream>
#include "TString.h"

void CreatePulseHeightPlot(){

    auto dman = DataManager("./data/");

    //Create Output File
    TFile *f = new TFile("pulseheight.root", "recreate");
    f->mkdir("Signals");
    f->mkdir("Signals_1d");
    f->mkdir("ADC");

    //Load the TRD calibration
    int runNumber = 526689;
    auto& ccdbMgr = o2::ccdb::BasicCCDBManager::instance();
    ccdbMgr.setURL("https://alice-ccdb.cern.ch/");
    auto runDuration = ccdbMgr.getRunDuration(runNumber);
    o2::trd::Calibrations simcal;
    simcal.getCCDBObjects(runDuration.first);
  
    //Initialize
    int ntb = 30;    //Number of time bins
    int ncols = 144; //Number of pads
    int adcvalues[ncols][ntb]; //ADC values of all pads in all time bins for one row
    int adcsum[ncols];        //Sum of ADC values in each pad over all time bins
    int adcrms[ncols];        //RMS of ADC values in each pad over all time bins
    int adcmean[ncols];       //Mean of ADC values in each pad over all time bins
    int adcstd[ncols];        //Std of ADC values in each pad over all time bins
    int adcmaxvalue[ncols];   //Maximum ADC value in each pad
    int adcmaxtb[ncols];      //Time bin containing ADC value in each pad
    int roi_mask[ncols];      //Mask that shows which pads belong to Region of Interest in row
    int max_col = -1;       //Pad with the highest ADC sum in row
    int max_col_value = -1; //ADC sum of pad with the highest ADC sum in row
    int adc[ntb];            //Single pad with all ADC values
    int n_rois = 0;         //RoIs found 

    //Initialize histograms
    TH1D *php = new TH1D("pulse-height", "pulse-height", ntb, -0.5, ntb-0.5);         //Histogram containing the pulse-height plot
    TH2D *tb_adc = new TH2D("tb_adc", "tb_adc", ntb, -.5, ntb-0.5, 1000, -.5, 999.5); //Histogram of ADC values for each time bin
    TH2D *signal;    //Histogram showing Region of Interest with each time bin
    TH1D *signal_1d; //Signal by projection onto 1 dimension

    TH1D *adcvalues_hist = new TH1D("adcvalues", "adcvalues", 10000, -.5, 9999.5); //Distribution of ADC values
    TH1D *adcsum_hist = new TH1D("adcsum", "adcsum", 10000, -.5, 9999.5);          //Distribution of ADC sums
    TH1D *adcrms_hist = new TH1D("adcrms", "adcrms", 10000, -.5, 9999.5);          //Distribution of ADC rms
    TH1D *adcstd_hist = new TH1D("adcstd", "adcstd", 10000, -.5, 9999.5);          //Distribution of ADC std
    TH1D *adcmax_hist = new TH1D("adcmax", "adcmax", 10000, -.5, 9999.5);          //Distribution of ADC max values

    //Set cuts
    float vDriftCut = 1.3;              //vDrift of detector
    float lowValueNoiseCut = ntb*20.;   //low value noise cut
    float highValueNoiseCut = ntb*200.; //high value noise cut
    float stripeCut = 7.;               //cut to remove stripes seen in ADC value histogram
  
    //Loop over timeframes
    int tfno = 0;
    while (dman.NextTimeFrame()){

      //Loop over events
      int evno = 0;
      while(dman.NextEvent()){

        auto ev = dman.GetEvent();
        if (ev.digits.length()<4){continue;} //RoI contain 4 pad rows, i.e. sort out events with less than 4 digits
        
        //Match to TPC/ITS-TPC tracks
        //For this we assume a digit is matched to a track if it is in the same detector and row as a tracklet matched
        //And in the same, adjacent, or next-to-adjacent pad column
        /*std::set<std::tuple<Int_t, Int_t, Int_t>> padcols; //get information of tracklets in that event
          
        for(auto &track : ev.trdtracks_tpc){
          //if(track.getHasPadrowCrossing()){continue;} //Discard if track has padrow crossings in any layer
          for(Int_t i_Layer=0; i_Layer<6; i_Layer++){
            if(track.getIsCrossingNeighbor(i_Layer)){continue;} //Discard if track has padrow crossing in that layer
            if(track.getTrackletIndex(i_Layer)==-1){continue;} //Disregard if track has no matching tracklet in that layer
            Int_t index = track.getTrackletIndex(i_Layer) - ev.firstTracklet; //Get index of tracklet matched to that track
            o2::trd::Tracklet64 tracklet = ev.tracklets.begin()[index];
            std::tuple<Int_t, Int_t, Int_t> pcol = {tracklet.getDetector(), tracklet.getPadRow(), tracklet.getPadCol()};
            padcols.insert(pcol); //add tracklet information
          }
        }*/
        
        //Partition data into continuous regions (as defined by DataManager)
        for(auto &[key, region] : RawDataPartitioner<ClassifierByContinuousRegion>(ev)){

          //Only regions with more/equal 4 digits can constitute rois
          if(region.digits.length()<4){continue;}

          //Reset everything
          for(int col=0; col<ncols; col++){
            roi_mask[col]=0;
            for(int tb=0; tb<ntb; tb++){
              adcvalues[col][tb] = 0;
            }
            adcsum[col] = 0;
            adcrms[col] = 0;
            adcmean[col] = 0;
            adcstd[col] = 0;
            adcmaxvalue[col] = 0;
            adcmaxtb[col] = 0;
          }
          max_col = -1;
          max_col_value = -1;

          for(int tb=0; tb<ntb; tb++){
            adc[tb] = 0;
          }

          //std::tuple<Int_t, Int_t, Int_t> pcol; //Digit information for matching to tracks
          
          //Fill arrays of pad columns with adc values, sums, rms, max values, and max tbs
          for(auto &digit : region.digits){

            //Digit detector and padrow for matching to tracks
            /*std::get<0>(pcol) = digit.getDetector();
            std::get<1>(pcol) = digit.getPadRow();*/
            
            if(simcal.getVDrift(digit.getDetector(), digit.getPadCol(), digit.getPadRow())<vDriftCut){continue;} //Discard if bad detector (i.e. vDrift too low)
            
            int max_tb = -1;
            int max_value = -1;

            for(int tb=0; tb<ntb; tb++){
              adcvalues[digit.getPadCol()][tb] = digit.getADC()[tb];
              adcrms[digit.getPadCol()] += TMath::Power(digit.getADC()[tb], 2);
              if(digit.getADC()[tb] > max_value){
                  max_tb = tb;
                  max_value = digit.getADC()[tb];
              }
            }

            adcsum[digit.getPadCol()] = digit.getADCsum();
            adcmean[digit.getPadCol()] = adcsum[digit.getPadCol()]/ntb;
            adcrms[digit.getPadCol()] = TMath::Sqrt(adcrms[digit.getPadCol()]/ntb);
            adcmaxtb[digit.getPadCol()] = max_tb;
            adcmaxvalue[digit.getPadCol()] = max_value;

            for(int tb=0; tb<ntb; tb++){
              adcstd[digit.getPadCol()] += TMath::Power(digit.getADC()[tb] - adcmean[digit.getPadCol()], 2);
            }

            adcstd[digit.getPadCol()] = TMath::Sqrt(adcstd[digit.getPadCol()]/29.);

          }

          //Fill the corresponding histograms
          for(int col=0; col<ncols; col++){
              if(adcsum[col]==0){continue;}
              for(int tb=0; tb<ntb; tb++){
                adcvalues_hist->Fill(adcvalues[col][tb]);
              }
              adcsum_hist->Fill(adcsum[col]);
              adcrms_hist->Fill(adcrms[col]);
              adcstd_hist->Fill(adcstd[col]);
              adcmax_hist->Fill(adcmaxvalue[col]);
          }    

          //Find ROI, that is digit with highest adcsum,
          //neighbour with highest adc sum, and neighbours of those two
          max_col = -1;
          max_col_value = -1;
          
          for(int col=0; col<ncols; col++){
            if(adcsum[col]>max_col_value){
              max_col = col;
              max_col_value = adcsum[col];
            }
          }
            
          roi_mask[max_col] = 1;
          roi_mask[max_col + 1] = 1;
          roi_mask[max_col - 1] = 1;
          roi_mask[adcsum[max_col - 1] > adcsum[max_col + 1] ? max_col - 2 : max_col + 2] = 1;
          
          //Check if digit has match to track
          //By checking if the is a matched tracklet with same detector, row, and column (+-2) as digit
          /*std::get<2>(pcol)=max_col;
          bool hasCorrespondingTrack = false;
          if(padcols.find(pcol) != padcols.end()){hasCorrespondingTrack = true;}
          std::get<2>(pcol)=max_col+1;
          if(padcols.find(pcol) != padcols.end()){hasCorrespondingTrack = true;}
          std::get<2>(pcol)=max_col+2;
          if(padcols.find(pcol) != padcols.end()){hasCorrespondingTrack = true;}
          std::get<2>(pcol)=max_col-1;
          if(padcols.find(pcol) != padcols.end()){hasCorrespondingTrack = true;}
          std::get<2>(pcol)=max_col-2;
          if(padcols.find(pcol) != padcols.end()){hasCorrespondingTrack = true;}
          if(!hasCorrespondingTrack){continue;}*/
            
          //Quality cuts (question: make quality cuts before or after finding ROI?)
          //Remove low value noise, high value noise and stripes
          for(int col=0; col<ncols; col++){
              bool cut = false;
              if(!roi_mask[col]){continue;}
              if(adcsum[col]<lowValueNoiseCut){cut = true;} //Cut low value noise
              //if(adcsum[col]>highValueNoiseCut){cut = true;} //Cut high value noise
              if(adcstd[col]<stripeCut){cut = true;} //Cut stripes

              //Remove stripe that appears in every third time bin
              float tb_3_std = 0;
              float tb_3_mean = 0;
              for(int tb=2; tb<ntb; tb+=3){
                  tb_3_mean += adcvalues[col][tb];
              }
              tb_3_mean /= ntb;
              for(int tb=2; tb<ntb; tb+=3){
                tb_3_std += TMath::Power(adcvalues[col][tb] - tb_3_mean, 2);
              }
              tb_3_std = TMath::Sqrt(tb_3_std/10.);
              if(tb_3_std<stripeCut){cut = true;}

              //When pad is to be removed, replace all entries with 0
              if(!cut){continue;}
              //if(adcsum[col]>600 && adcsum[col]<6000){continue;}
              for(int tb=0; tb<ntb; tb++){
                adcvalues[col][tb] = 0;
              }
                adcsum[col] = 0;
          }

          //Skip if all digits are 0 (that is did not survive quality cuts)
          bool contains_digits = false;
          for(int col=0; col<ncols; col++){
            if(!roi_mask[col]){continue;}
            if(adcsum[col]!=0){contains_digits = true;}
          }
          if(!contains_digits){continue;}
          

          //Fill the histograms
          //Distribution of ADC values in each time bin
          for(int col=0; col<ncols; col++){
            if(!roi_mask[col]){continue;}
            for(int tb=0; tb<ntb; tb++){
              adc[tb] += adcvalues[col][tb];
            }
          }

          //Make histogram to show the signal
          signal = new TH2D(Form("signal_%i_%i_%i", tfno, evno, n_rois), Form("signal_%i_%i_%i", tfno, evno, n_rois), ntb, -.5, ntb-.5, 4, -.5, 3.5);
          int roi_digit = 0;
          for(int col=0; col<144; col++){
            if(!roi_mask[col]){continue;}
            roi_digit++;
            for(int tb=0; tb<ntb; tb++){
              signal->SetBinContent(tb+1, roi_digit, adcvalues[col][tb]);
            }
          }
          f->cd("Signals");
          signal->Write();
          
          //Make 1d signal
          signal_1d = new TH1D(Form("signal_1d_%i_%i_%i", tfno, evno, n_rois), Form("signal_1d_%i_%i_%i", tfno, evno, n_rois), ntb, -.5, ntb-0.5);
          for(int tb=0; tb<ntb; tb++){
            signal_1d->SetBinContent(tb+1, adc[tb]);
          }
          f->cd("Signals_1d");
          signal_1d->Write();
          
          //Fill pulse-height plot
          for(int tb=0; tb<ntb; tb++){
            php->SetBinContent(tb+1, php->GetBinContent(tb+1) + adc[tb]);
            tb_adc->Fill(tb, adc[tb]);
          }

          n_rois++;

        }

        evno++;
      }

      tfno++;

    }

    //Get pulse-height plot by dividing over number of RoIs
    for(int tb=1; tb<=ntb; tb++){
      php->SetBinContent(tb, php->GetBinContent(tb)/n_rois);
    }

    //Save number of RoIs in 0th bin
    php->SetBinContent(0, n_rois);

    f->cd();
    php->Write();
    tb_adc->Write();
    f->cd("ADC");
    adcvalues_hist->Write();
    adcsum_hist->Write();
    adcrms_hist->Write();
    adcstd_hist->Write();
    adcmax_hist->Write();
    f->Close();
}